/******************************************************************************
* M1 Communication Inc.
* (c) Copyright 2014 M1 Communication Inc.
* ALL RIGHTS RESERVED.
***************************************************************************/
/*!
 * \file      sms_sim.c
 * \author    Hoang Y
 * \date      August-09-2021
 */

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <tchar.h>
#include <windows.h>

/******************************************************************************
* Constants and macros
******************************************************************************/

#define PIPENAME "\\\\.\\pipe\\pipe-named"
#define PASSWORD "hoangy99"

HANDLE fileHandle;

typedef void (*PACKET_FUNC_PTR)(char*);
typedef struct parse_sms_process_tag{

	const char					packet_str[15];
	PACKET_FUNC_PTR 			func_ptr;

}PARSE_PACKET_PROCESS_T;

/******************************************************************************
* Local function prototypes
******************************************************************************/
void sms_Start(char* value);

void sms_Reboot(char* value);

void sms_Test(char* value);

const PARSE_PACKET_PROCESS_T packet_process_table[] =
{
		{"START",sms_Start},
		{"REBOOT",sms_Reboot},
		{"TEST",sms_Test}
};

/******************************************************************************
* Local functions
******************************************************************************/

void sms_Start(char* input)
{
	printf("bat dau chuong trinh\n");
}

void sms_Reboot(char* input)
{
	printf("chay lai chuong trinh\n");
}

void sms_Test(char* input)
{
	printf("day la test\n");
}

void sms_Process(char* input)
{
	int i;
	int arr_size = sizeof(packet_process_table)/sizeof(packet_process_table[0]);
	for(i = 0; i < arr_size; i++)
	{
		if(strcmp((char*)packet_process_table[i].packet_str,input) == 0)
		{
			packet_process_table[i].func_ptr(input);
			break;
		}
	}
}

/******************************************************************************
* Global functions
******************************************************************************/
/*!
 * ReadString function
 * \brief read string from fileHandle
 * ReadFile()
 * \return string read
 */
void ReadString(char* output) {
  ULONG read = 0;
  int32_t index = 0;
  do
  {
	  ReadFile(fileHandle, output + index++, 1, &read, NULL);
  }
  while (read > 0 && *(output + index - 1) != 0);
}

/*!
 * sms_isNewSMS function
 * \brief Notification of new SMS received
 * print notification
 */
void sms_isNewSMS(int32_t leng_SMS)
{
	if (leng_SMS == 0)
		printf("\nKhong co tin nhan!\n");
	else
		printf("\nCo tin nhan moi!\n");
}

/*!
 * sms_ReadData function
 * \brief read data from input string
 */
void sms_ReadData(char* buffer, int32_t leng_SMS)
{
	if (leng_SMS != 0)
	{
		printf("read data from server: %s\n",buffer);
	}
}

/*!
 * sms_WtrieData function
 * \brief write data to file and send
 * WriteFile()
 */
void sms_WriteData(const char* contentMessage)
	{
		DWORD numWritten;
		WriteFile(fileHandle,
				contentMessage,
				strlen(contentMessage),
				&numWritten,
				NULL);
	}

void sms_CreateKey(char* Key)
{
	char* _Key = PASSWORD;
	while(strcmp(_Key,PASSWORD) == 0)
	{
		sms_WriteData("hoangy99");
		ReadString(Key);
		_Key = Key;
	}
	printf("key da duoc thay doi thanh: %s\n",Key);
}

void sms_Login(char* Key)
{
	sms_WriteData(Key);
}
/*!
 * Main function
 * Ref CreateFile(),strset(),ReadString(),sms_ReadData(),sms_WriteData()
 * Ref sms_isNewSMS()
 */
int main()
{
	while (1)
	{
  	// create file
	fileHandle = CreateFile(TEXT(PIPENAME),
							  GENERIC_READ | GENERIC_WRITE,
							  FILE_SHARE_WRITE,
							  NULL,
							  OPEN_EXISTING,
							  0,
							  NULL);
//	while (1)
//	{
//	char* a = (char*)malloc(10);
//	a = "hoangy99";
//	noichuoi(a);
//	printf("chuoi dc noi la: %s",a);
//	// send data to server
//	const char* contentMsg = "hello from c++\r\n";
//	sms_WriteData(contentMsg);
//	
//	// read from pipe server
	char* buffer = (char*)malloc(100);
	strset(buffer, 0);
//	ReadString(buffer);
//
//	//notification of new SMS received
//	int32_t lengSMS = strlen(buffer);
//	sms_isNewSMS(lengSMS);
//	sms_ReadData(buffer,lengSMS);	
//
//	//parse string and implement
//	sms_Process(buffer);
//	Sleep(5000);
//	free(buffer);
	sms_CreateKey(buffer);
	//break;
	Sleep(5000);
	}
}

