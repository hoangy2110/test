﻿using System;
using System.IO;
using System.Threading;
using System.IO.Pipes;
namespace pipelines
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"Key.txt";
            var namedPipeServer = new NamedPipeServerStream("pipe-named", PipeDirection.InOut, 1, PipeTransmissionMode.Byte);
            var streamReader = new StreamReader(namedPipeServer);
            namedPipeServer.WaitForConnection();
            //for(; ;)
            //{
                char[] c = null;
                c = new char[8];
                int i = 0;
                while(streamReader.Peek() >= 0)
                {
                    c[i] = (char)streamReader.Read();
                    i++;
                }
                string msg = new string(c);
                Console.WriteLine($"read from pipe client: {msg}");
                // using (StreamWriter sw = new StreamWriter(path))
                // {
                //     sw.WriteLine("msg");
                // }
                using (StreamReader sr = new StreamReader(path))
                {
                    while(sr.Peek() >= 0)
                    {
                        if(sr.ReadLine() == msg)
                        {
                            var writer = new StreamWriter(namedPipeServer);
                            writer.Write("12345678");
                            writer.Write((char)0);
                            writer.Flush();
                            namedPipeServer.WaitForPipeDrain();
                        }
                    }
                }
                //namedPipeServer.Dispose();
                //Thread.Sleep(2000);
            //}
        }
    }
}
