%DFT
x = [];
y = fft(x);

%IDFT
x1 = [];
y1 = ifft(x1);

%chap vong
x2 = [];
y2 = [];
N = [];
clin = cconv(x2,y2,N);

%Hanning
M = 5;
wc = pi/3;
h = zeros(1,M);
for k = 0:M-1
    hd = (-wc/pi) * sinc(1/3*(k-(M-1)/2));
    w = 0.5-0.5*cos(2*pi*k/(M-1));
    h(k+1) = hd * w;
end

%Hamming
M1 = 5;
wc1 = pi/3;
h1 = zeros(1,M1);
for k1 = 0:M1-1
    hd1 = (-wc1/pi) * sinc(1/3*(k1-(M1-1)/2));
    w1 = 0.54-0.46*cos(2*pi*k1/(M1-1));
    h1(k1+1) = hd1 * w1;
end

%BlackMan
M2 = 5;
wc2 = pi/3;
h2 = zeros(1,M2);
for k2 = 0:M2-1
    hd2 = -wc2/pi * sinc(1/3*(k2-(M2-1)/2));
    w2 = 0.42-0.5*cos(2*pi*k2/(M2-1))+ 0.08*cos(4*pi*k2/(M2-1));
    h2(M2+1) = hd2 * w2;
end